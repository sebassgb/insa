<?php
use PHPUnit\Framework\TestCase;

require_once 'src/Poneys.php';

/**
 * Classe de test de gestion de poneys
 */
class unitTest extends TestCase
{
    private $Poneys;
    /**
     * Undocumented function
     *
     * @return void
     */
    public function testRemovePoneyFromField()
    {
        // Action
        $this->Poneys->removePoneyFromField(3);
        // Assert
        $this->assertEquals(5, $this->Poneys->getCount());
    }

    public function setUp()
    {
        $this->Poneys = new Poneys();
        $this->Poneys->setCount(8);
    }

    public function tearDown(){
        unset($this->poneys);
    }

}
?>

   

