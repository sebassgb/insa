<?php
use PHPUnit\Framework\TestCase;

require_once 'src/Poneys.php';

/**
 * Classe de test de gestion de poneys
 */
class PoneysTest extends TestCase
{
        /**
     * @dataProvider provider
     */
  
  
        public function testRemovePoneyFromField($count, $remove)
    {
        // Setup
        $Poneys = new Poneys();

        // Action
        $Poneys->removePoneyFromField($remove);

        // Assert
        $this->assertEquals($count, $Poneys->getCount());
    }
      /**
     * Undocumented function
     *
     * @return void
     */
    public function provider()
    {
        return array(
          array(3, 5),
          array(0, 8),
          array(1, 7),
          array(2, 6)
        );
    }
  

}
?>
