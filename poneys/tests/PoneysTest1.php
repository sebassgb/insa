<?php
use PHPUnit\Framework\TestCase;

require_once 'src/Poneys.php';

/**
 * Classe de test de gestion de poneys
 */
class PoneysTest extends TestCase
{
    /**
     * Undocumented function
     *
     * @return void
     */
   /* public function testRemovePoneyFromField()
    {
        // Setup
        $Poneys = new Poneys();

        // Action
        $Poneys->removePoneyFromField(3);

        // Assert
        $this->assertEquals(5, $Poneys->getCount());
    }
*/

    public function testAddPoneyToField()
    {
        // Setup
        $Poneys = new Poneys();
        $Poneys->addOnePoneyToField();

        // Action
        //$Poneys->removePoneyFromField(3);

        // Assert
        $this->assertEquals(9, $Poneys->getCount());
    }
}
?>
