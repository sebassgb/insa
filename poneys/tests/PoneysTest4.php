<?php
use PHPUnit\Framework\TestCase;

require_once 'src/Poneys.php';

/**
 * Classe de test de gestion de poneys
 */
class PoneysTest extends TestCase
{
    /**
     * Undocumented function
     *
     * @return void
     *//*
    public function testRemovePoneyFromField()
    {
        // Setup
        $Poneys = new Poneys();

        // Action
        $Poneys->removePoneyFromField(3);

        // Assert
        $this->assertEquals(5, $Poneys->getCount());
    }
*/
    public function test_getNames()
{
    $names = ['joe', 'william', 'jack'];

    $this->poneys = $this->getMockBuilder('Poneys')->getMock();
    $this->poneys
    ->expects($this->exactly(1))
    ->method('getNames')
    ->willReturn($names);

    $this->assertEquals(
        $names,
        $this->poneys->getNames()
    );  
}
}

?>

