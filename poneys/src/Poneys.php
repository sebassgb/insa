<?php
/**
 * Gestion de poneys
 */
class Poneys
{
    private $count;

    public function setCount($number): void
    {
        $this->count = $number;
    }

    /**
     * Retourne le nombre de poneys
     *
     * @return void
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * Retire un poney du champ
     *
     * @param int $number Nombre de poneys à retirer
     *
     * @return void
     */
    public function removePoneyFromField(int $number): void
    {
        if ($number > $this->getCount()) {
            throw new Exception('imposible we do not have that many poneys');
        }
        else{
            $this->count -= abs($number);
        }
        
    }

    public function addOnePoneyToField(): void
    {
        $this->count ++;
    }

    public function addPoneysToField($number): void
    {
        if($this->verifySpaceInField(abs($number))==true){
            $this->count += abs($number);

        }
    
    }

    public function verifySpaceInField($number): bool
    {
        if ($this->getCount() ==15 || ($this->getCount()+$number)>15 ) {
            throw new Exception('imposible we do not have space for more poneys');
            return false;
        }
     return true;
    }



    /**
     * Retourne les noms des poneys
     *
     * @return array
     */
    public function getNames(): array
    {

    }
}
?>
